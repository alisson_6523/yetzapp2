/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { View,   Text } from 'react-native';
import { WebView } from 'react-native-webview';




export default class App extends Component{
  constructor(props){
    super(props)
  }

  render(){
    return (
      <WebView
        source={{ uri: 'https://www.yetzcards.com.br/' }}
        style={{ marginTop: 21 }}
      />   
    )
  }
}
